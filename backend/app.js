const express = require('express')
const app = express()
const cors = require('cors');

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.use(cors())

const port = 443;

var dict = {};

app.get('/results', (req, res) => {
    var response = {};
    console.log("Sending votes so far");
    for(var key in dict){
        console.log("Adding key " + key);
        response[key] = dict[key].toString();
    }  
    res.send(JSON.stringify(response));
});

app.post('/vote', (req,res) => {
    console.log("here");
    vote = req.body.option;
    if(vote in dict){
        dict[vote] = dict[vote] + 1;
        console.log("number of votes is: " + dict[vote]);
    } else {
        dict[vote] = 1;
        console.log("Added new vote type " + vote);
    }
    console.log(dict);
    res.send("");
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`))