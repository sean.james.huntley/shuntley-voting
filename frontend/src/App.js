import React, { Component } from 'react'
import axios from 'axios';
import VMwareLogo from './assets/vmware.svg'
import './App.css'

import Poll from 'react-polls'

const backend = process.env.REACT_APP_BACKENDIP

console.log(backend)

const pollQuestion1 = 'Which VMware products have you heard of?';
const option1 = "vSphere";
const option2 = "VMware Workstation / Fusion";
const option3 = "NSX";

// const pollQuestion1 = 'Which is the best univeristy?';
// const option1 = "USYD";
// const option2 = "UNSW";
// const option3 = "UTS";

var pollAnswers1 = [
  { option: option1, votes: 0 },
  { option: option2, votes: 0 },
  { option: option3, votes: 0 }
]

const pollStyles1 = {
  questionSeparator: true,
  questionSeparatorWidth: 'question',
  questionBold: true ,
  questionColor: '#303030',
  align: 'center',
  theme: 'purple'
}

export default class App extends Component {
  constructor(props){
    super(props);
    this.timeout =0;
    this.state = {
      pollAnswers1: [...pollAnswers1],
    }
  }
  componentDidMount(){
    document.title = "CAS Demo"
  }

  handleVote = (voteAnswer, pollAnswers, pollNumber) => {
    const newPollAnswers = pollAnswers.map(answer => {
      if (answer.option === voteAnswer) answer.votes++
      return answer
    })

    if (pollNumber === 1) {
      this.setState({
        pollAnswers1: newPollAnswers
      })
    }
  }

  componentWillMount() {
    this.getResults();
    this.timeout = setTimeout( () => {
      this.componentWillMount();
    }, 1000
    );
  }

  getResults(){
    var pollAnswers1 = [
      { option: option1, votes: 0 },
      { option: option2, votes: 0 },
      { option: option3, votes: 0 }
    ]
    axios.get('http://' + backend +':443/results')
    .then(res => {
    var votes = res.data;
    for(var key in res.data){
      for(var i=0;i<votes[key];i++){
        this.handleVote(key, pollAnswers1, 1)
      }
    }
    })
  }

  componentWillUnmount() {
    clearTimeout(this.timeout);
  }

  sendVote(voteAnswer){
    var data = {}
    data["option"] = voteAnswer;
    axios.post('http://' + backend +':443/vote',data)
    .then(res => {
        this.handleVote(voteAnswer, pollAnswers1, 1)
    })
  }


  render () {
    const { pollAnswers1} = this.state

    return (
      <div className='app'>
        <header className='header'>
          <h1 className='name'>Realise What's Possible</h1>
          <img src={VMwareLogo} className='logo' alt='VMware Logo' />
          <h1 className='name'> </h1>
        </header>
        <main className='main'>
          <div>
            <Poll question={pollQuestion1} answers={pollAnswers1} onVote={voteAnswer => this.sendVote(voteAnswer)} customStyles={pollStyles1} noStorage />
          </div>
        </main>

      </div>
    )
  }
}
